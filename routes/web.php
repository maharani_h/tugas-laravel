<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@bio');

Route::post('/welcome', 'AuthController@submitbio');

Route::get('/data-tables', function(){
    return view('table.data-tables');
});

// CRUD Cast
// Create
Route::get('/cast/create', 'CastController@create'); //route menuju form untuk pemain baru
Route::post('/cast', 'CastController@store'); //route untuk menyimpan form pemain baru ke database

//Read
Route::get('/cast', 'CastController@index'); //route menuju index
Route::get('/cast/{cast_id}', 'CastController@show'); //route untuk menampilkan detail data pemain film dengan id tertentu

//Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //route untuk menampilkan form untuk edit pemain film dengan id tertentu
Route::put('/cast/{cast_id}', 'CastController@update'); //route untuk menyimpan perubahan data pemain film (update) untuk id tertentu

//Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //route untuk menghapus data pemain film dengan id tertentu
