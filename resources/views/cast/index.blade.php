@extends('layout.master')

@section('judul')
Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-secondary mb-3">Tambah Profil</a>

<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">No.</th>
        <th scope="col">Nama Lengkap</th>
        <th scope="col">Usia</th>
        <th scope="col">Biodata</th>
        <th scope="col">Detail Profil</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>
                    
                    <form action="/cast/{{$item->id}}" method="POST">
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Klik di sini</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                </td>
            </tr>
            
        @empty
            <h1>Data Tidak Ditemukan</h1>
        @endforelse
    </tbody>
  </table>

@endsection