@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
<h2>Buat Account Baru</h2>
    <form action="/welcome" method="post">
        @csrf
       <h4>Sign Up Form</h4> 
        <label>First name :</label> <br> <br>
        <input type="text" name="fname"> <br> <br>
        <label>Last name :</label> <br> <br>
        <input type="text" name="lname"> <br> <br>
        <label>Gender</label> <br> <br>
        <input type="radio" value="g1" name="gdr">Male <br>
        <input type="radio" value="g2" name="gdr">Female <br> <br>
        <label>Nationality</label> <br> <br>
        <select name="nationality" id="">
            <option value="n1">Australian</option>
            <option value="n2">Brazilian</option>
            <option value="n3">Canadian</option>
            <option value="n4">Danish</option>
            <option value="n5">English</option>
            <option value="n6">French</option>
            <option value="n7">Greek</option>
            <option value="n8">Haitian</option>
            <option value="n9">Indonesian</option>
            <option value="n10">Japanese</option>
            <option value="n11">Malagasy</option>
            <option value="n12">Russian</option>
            <option value="n13">Zambian</option>
        </select> <br> <br>
        <label>Language Spoken</label> <br> <br>
        <input type="checkbox" value="ls1" name="lang">Bahasa Indonesia <br>
        <input type="checkbox" value="ls2" name="lang">English <br>
        <input type="checkbox" value="ls3" name="lang">Other <br> <br>
        <Label>Bio</Label> <br> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
@endsection