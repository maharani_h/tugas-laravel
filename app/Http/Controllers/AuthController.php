<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('form');
    }

    public function submitbio(Request $request){
        $nama1 = $request['fname'];
        $nama2 = $request['lname'];
        return view('welcomepage', compact('nama1', 'nama2'));
    }
}
